# Setting up Umbraco

* [Umbraco, docker and kubernetes - should we care?](https://skrift.io/issues/umbraco-docker-and-kubernetes-should-we-care/)
* [Install umbraco with .NET CLI](https://our.umbraco.com/documentation/Fundamentals/Setup/Install/install-umbraco-with-templates)
* [How to run Umbraco9 as Linux Docker Container](https://swimburger.net/blog/umbraco/how-to-run-umbraco-9-as-a-linux-docker-container)


### Dotnet requirements and Umbraco Versions
Current version of Umbraco is 9, from what I could dig up from the Netorka installation, they are using Umbraco 7
* Umbraco7 requires .NET 4.5.0, and the [official docker images for .NET SDK](https://hub.docker.com/_/microsoft-dotnet-sdk) skip over 4 as a version.
* It seems that Umbraco9 has the requirement of .NET 5

## Dotnet Templates
We need to install the templates we wish to use. 
* Install templates for dotnet
    >Guide used following command to install templates for umbraco
    ```
    dotnet new -i Umbraco.Templates::*
    ```
* List available templates
    >It can be nice to see what has been installed and how you can call on it. 
    ```
    dotnet new -l
    ```
* Create new project from template
    >You can either create the directory first and move into it, then create the project
    ```
    dotnet new umbraco
    ```
    OR simply name the project using the `-n` flag, this creates a directory for it.
    ```
    dotnet new umbraco -n Umbraco9Docker
    ```

## Database
Prerequisites for setting up Umbraco is to have a database, and it uses a __connection string__ to interact with that database.

* [Connection string settings](https://our.umbraco.com/documentation/reference/V9-Config/ConnectionStringsSettings/)
* [Microsoft SqlClient Data Provider for SQL Server](https://www.connectionstrings.com/sql-server-2019/)
* [ASP.NET Core + SQL Server on Linux (docker-compose)](https://docs.docker.com/samples/aspnet-mssql-compose/)
* [Run Microsoft SQL server with docker](https://citizix.com/how-to-run-mssql-server-2019-with-docker-and-docker-compose/)

```
Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=myPassword;
```
Since I aim to have this running in Docker containers, I found the [Microsoft SQL Server](https://hub.docker.com/_/microsoft-mssql-server) official docker image.

>I used the VSCode search to find out where the connection string goes into the Umbraco installation, it is a variable called `umbracoDbDSN`, it is located in the `appsettings.json` in Umbraco9, but seems to at least have same variable name in older versions, but found in other places.

Since it is actually sensitive information, I figured trying to use environment variables through the `docker-compose` file, and just have `"$dbConnectionString"` within the `appsettings.json`

>Shouldn't need to define ports for the database though... containers run by the _docker-compose_ are automatically on the same network. And added a volume that would map a local directory `./db` to be used by the database (allows us to have persistent data)

## Docker
I set up a `docker-compose.yaml` because it's wonderful, won't run containers without it! 

* To start up the containers
    ```
    docker-compose up -d
    ```
    >Can specify a service, then it only starts up that container and anything it depends on. `-d` is detached mode (not stuck to the given terminal)
    ```
    docker-compose up <service-name>
    ```
* To rebuild images
    ```
    docker-compose build
    ```
    >Same as with previous command, can specify which service you are rebuilding image of
* To tear down containers 
    >It stops the containers, removes them. [Reference](https://docs.docker.com/compose/reference/down/)
    ```
    docker-compose down
    ```
    There are extra flag options allows you to also remove associated images, volumes and orphan containers. Makes it easy to clean up.

## Issues
1. The umbraco container would always shut down
    ```
    Process terminated. Failed to load app-local ICU: libicudata.so.68.2
    ```
    * [.NET globalization and ICU](https://docs.microsoft.com/en-us/dotnet/core/extensions/globalization-icu)
    * [install and manage packages using dotnet CLI](https://docs.microsoft.com/en-us/nuget/consume-packages/install-use-packages-dotnet-cli)
    * [icu-libs in SDK but nt runtime](https://github.com/dotnet/dotnet-docker/issues/2365)
    * [.NET Core, Docker, and Cultures](https://andrewlock.net/dotnet-core-docker-and-cultures-solving-culture-issues-porting-a-net-core-app-from-windows-to-linux/)
    > Starting to sound like it's an issue with creating the application in windows and then running it on linux (in container). But the premade docker images of dotnet SDK are linux...

1. Finally caved in and tried this: 
    [Docker Umbraco 9](https://github.com/darrenferguson/docker-umbraco-9)
    > It creates the Umbraco installation from within the container, but also the server within the same container.
    * Seems a bit heavy, and you can't access the Umbraco files (they are not local, only in container)
    * Adjusted it so it copied a local Umbraco project, rather than creating it new in container. Then everything fails.
1. Decided to disect what the above attemt does correctly, such as the format of the _connection string_ should be like. It might solve things.
    >Going to try out setting up the Umbraco using dotnet through linux (WSL) and see if that makes a difference. If somehow it's realated to creating project in Windows and then running on Linux in container (Yeah, shouldn't matter... it's not an executable...).
    * Ok, seems to have done something, getting different error now (probably implies the connection string is also correct this time around)
        ```
        Unhandled exception. System.IO.DirectoryNotFoundException: /app/wwwroot/media/
        ```
    * Decided to keep following [this guide](https://swimburger.net/blog/umbraco/how-to-run-umbraco-9-as-a-linux-docker-container) since the connection to DB seems to be established.
        * Added the volume for persistent files

## Versions of .NET SDK
* You can actually have multiple versions and specify which one to use, Umbraco wants 5 it seems. The following command lists all the versions you have installed.
    ```
    dotnet --list-sdks
    ```
* [Select the .NET version to use](https://docs.microsoft.com/en-us/dotnet/core/versions/selection)
    >By using that `global.json` trick to specify version, the dotnet SDK will switch between. 
    ```sh
    PS F:\School\HR\22_haust\lokaverkefni\umbracoTest> dotnet --version
    6.0.200
    PS F:\School\HR\22_haust\lokaverkefni\umbracoTest> cd app
    PS F:\School\HR\22_haust\lokaverkefni\umbracoTest\app> dotnet --version
    5.0.406
    PS F:\School\HR\22_haust\lokaverkefni\umbracoTest\app>
    ```
